module authService

go 1.18

require (
	github.com/bshuster-repo/logrus-logstash-hook v1.0.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20220511200225-c6db032c6c88
)

require golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/lib/pq v1.10.5 // indirect
	github.com/rs/cors v1.8.2
	github.com/sirupsen/logrus v1.8.1
)
